package testing;

public class Calculator {
	
	//Sum of two numbers
	public int sum(int n1, int n2) {
		return n1+n2;
	}
	//Difference of two numbers
	public int diff(int n1, int n2) {
		return n1-n2;
	}
	//Divide two numbers
	public int divide(int n1, int n2) {
		return n1/n2;
	}
	//Multiply two numbers
	public int multiply(int n1, int n2) {
		return n1*n2;
	}
}
