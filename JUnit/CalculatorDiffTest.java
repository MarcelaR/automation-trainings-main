package testing;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CalculatorDiffTest {

	@Test
	void test() {
		Calculator test = new Calculator();
		int output = test.diff(10, 8);
		assertEquals(2,output);
	}

}
