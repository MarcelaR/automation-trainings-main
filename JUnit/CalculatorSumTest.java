package testing;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CalculatorSumTest {

	@Test
	void test() {
		Calculator test = new Calculator();
		int output = test.sum(3, 8);
		assertEquals(11,output);
	}

}
