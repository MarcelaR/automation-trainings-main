package testing;

import static org.junit.jupiter.api.Assertions.*;
import java.util.Arrays;
import java.util.Collection;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;


@RunWith(Parameterized.class)
public class CalculatorMultiplyTest {
	private Calculator calculator;
	private int n1;
	private int n2;
	private int expectedResult;
	
	public CalculatorMultiplyTest(int n1, int n2, int expectedResult) {
		this.calculator = new Calculator();
		this.n1 = n1;
		this.n2 = n2;
		this.expectedResult = expectedResult;
	}
	@Parameterized.Parameters
	public static Collection<Object[]> data(){
		return Arrays.asList(new Object[][] {{5,10,50},{2,8,16}});
	}
    @Test
    public void multiplyTest() {
        int result = calculator.multiply(n1, n2);
        assertEquals(result,expectedResult);
    }


}
