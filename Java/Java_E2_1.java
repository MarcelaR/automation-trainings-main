
//Print Fibonacci series upto specific number

import java.util.Scanner;

public class Java_E2_1 {

	public static void main(String args[]) { 
		System.out.println("Fibonacci series to print: "); 
		int count = new Scanner(System.in).nextInt(); 
		System.out.println("Fibonacci series: "); 

		int y = 1; int z = 1; int x = 0;
		
		if (count>0) {
			for(int i=1; i<=count; i++) {
				z = x + y;
				System.out.print(z +" ");
				x = y;
				y = z;
			}
		}
	} 
	
	public static int fibonacci(int count){ 
		if(count == 1 || count == 2){ 
			return 1; 
			} 
		return fibonacci(count-1) + fibonacci(count-2);
	}

}
