
public class Java_E_4 {

	public static void main(String[] args) {
		System.out.println("The greatest number is: " + getGreatestNumber(23,12,19,55));
		System.out.println("The greatest number is: " + getGreatestNumber(122,98,377));
	}
	public static int getGreatestNumber(int a, int b, int c, int d) {
		int num=Math.max(Math.max(a,d),Math.max(b, c));
		return num;
	}
	public static int getGreatestNumber(int a, int b, int c) {
		int num=Math.max(a,Math.max(b, c));
		return num;
	}
	}
