
public class Java_E_1 {
	
	public static void main(String[] args)
    {
		//1. Strings to concatenate
        	String str1 = "hot";
        	String str2 = "dog";
        	//Show the strings
        	System.out.println("String 1: " + str1);
        	System.out.println("String 2: " + str2); 
        	// Concatenate the strings and display result
        	String stringCon = str1.concat(str2);
        	System.out.println("Final string: " + stringCon);
       
    }
	

}
