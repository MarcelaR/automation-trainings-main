import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class students {  //class

	int StudentId;
	String FirstName;
	String MiddleName;
	String LastName;
	int DOB;

	public students(int i, String fn, String mn, String ln, int d)  //constructor
	{
		StudentId = i;
		FirstName = fn;
		MiddleName = mn;
		LastName =ln;
		DOB = d;
		
		String name = concatN(this.FirstName, this.MiddleName, this.LastName);
    	System.out.println("Student Name: " + name);
	}
	
	//Method to concatenate
	public String concatN(String n1, String n2, String n3) {
	    	String strCon = n1.concat(" ").concat(n2).concat(" ").concat(n3); 
	    	return strCon;
	}
	
	public void Bd (String BirthDate){
		String oldstring = BirthDate +" 00:00:00.0";
		LocalDateTime datetime = LocalDateTime.parse(oldstring, DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss.S"));
		String newstring = datetime.format(DateTimeFormatter.ofPattern("yyyy/MM/dd"));
		System.out.println("Birth Date: " + newstring);
	}
}
