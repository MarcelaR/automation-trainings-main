//Exception handling

public class Java_E2_2 {

	public static void main(String[] args) {
		
		// Divided by Zero
		try {
		      int dividedByZero = 10 / 0;
		}
		    
		catch (ArithmeticException e) {
			  System.out.println("It is not divisible");
		      System.out.println("ArithmeticException => " + e.getMessage());
		}
		
		//Concatenate with null
		try {
		      String str1=null;
		      String str2= "word";
		      System.out.println(str1.concat(str2));
		} 
		catch (Exception e) {
			  System.out.println("It is not possible to concatenate");
		      System.out.println(e.getMessage());
		}

	}

}
