
public class Java_E_3 {
	public static void main(String[] args) {
		int count= 25;
		printFibonacciNum(count);
	}
	public static void printFibonacciNum(int count) {
		int y = 1;
		int z = 1;
		int x = 0;
		
		if (count>0) {
			for(int i=1; i<=count; i++) {
				z = x + y;
				System.out.println(z);
				x = y;
				y = z;
			}
		}
	}
}
