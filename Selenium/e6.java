package introduction;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class e6 {
	

	private WebDriver driver;
	private Exercise5 readFile;
	private By searchText1Locator = By.id("first-name");
	private By searchText2Locator = By.id("last-name");
	private By searchText3Locator = By.id("job-title");
	
	@Before
	public void setUp() throws IOException, InterruptedException {
		
		//Screenshot lines
		Date currentdate = new Date();
		String screenshotfilename = currentdate.toString().replace(" ","-").replace(":","-");
		System.out.println(screenshotfilename);
		
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\merui\\OneDrive\\Escritorio\\HERBAL LIFE\\Cursos Udemy\\chromedriver.exe");
		driver = new ChromeDriver();
		readFile = new Exercise_6();
		
		driver.get("https://formy-project.herokuapp.com/form");
	}
	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
	
	@Test
	void test(String screenshotfilename) {
		
		String filepath ="C:\\Users\\merui\\OneDrive\\Escritorio\\Data1.xlsx";
		String searchText1 = readFile.getStringCellValue(filepath, "Sheet1",0,0);
		String searchText2 = readFile.getStringCellValue(filepath, "Sheet1",1,0);
		String searchText3 = readFile.getStringCellValue(filepath, "Sheet1",2,0);
		
		driver.findElement(searchText1Locator).sendKeys(searchText1);
        driver.findElement(searchText2Locator).sendKeys(searchText2);
		driver.findElement(searchText3Locator).sendKeys(searchText3);
		driver.findElement(By.id("radio-button-2")).click();
		//ss
		File screenshotFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(screenshotFile, new File (".//screenshot//"+screenshotfilename+"_1.png"));
		
		driver.findElement(By.id("checkbox-2")).click();
		//ss
		File screenshotFile2 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(screenshotFile2, new File (".//screenshot//"+screenshotfilename+"_2.png"));
		
		driver.findElement(By.xpath("//*[@id=\"select-menu\"]/option[2]")).click();
		driver.findElement(By.id("datepicker")).click();
		driver.findElement(By.className("today")).click();
		//ss
		File screenshotFile3 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(screenshotFile3, new File (".//screenshot//"+screenshotfilename+"_3.png"));
		
		
		driver.findElement(By.cssSelector("a[role='button']")).click();
		
		Thread.sleep(5000);
		//ss
		File screenshotFile4 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(screenshotFile4, new File (".//screenshot//"+screenshotfilename+"_4.png"));
		
		assertTrue(formPage.submit());
		submissionPage = new SubmissionPage(driver);
		assertEquals("The form was successfully submitted!", submissionPage.getSubmisionMessage());

	}
}
