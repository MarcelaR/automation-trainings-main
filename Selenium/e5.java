package introduction;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.SubmissionPage;
import pages.WebFormPage;
import introduction.Exercise5.ReadExcel;

public class e5 {
	
	private WebDriver driver;
	private Exercise5 readFile;
	private By searchText1Locator = By.id("first-name");
	private By searchText2Locator = By.id("last-name");
	private By searchText3Locator = By.id("job-title");
	
	@Before
	public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\merui\\OneDrive\\Escritorio\\HERBAL LIFE\\Cursos Udemy\\chromedriver.exe");
		driver = new ChromeDriver();
		readFile = new Exercise5();
		
		driver.get("https://formy-project.herokuapp.com/form");
	}
	@After
	public void tearDown() throws Exception {
		driver.quit();
	}
	
	@Test
	void test() {
		
		String filepath ="C:\\Users\\merui\\OneDrive\\Escritorio\\Data1.xlsx";
		String searchText1 = readFile.getStringCellValue(filepath, "Sheet1",0,0);
		String searchText2 = readFile.getStringCellValue(filepath, "Sheet1",1,0);
		String searchText3 = readFile.getStringCellValue(filepath, "Sheet1",2,0);
		
		driver.findElement(searchText1Locator).sendKeys(searchText1);
        driver.findElement(searchText2Locator).sendKeys(searchText2);
		driver.findElement(searchText3Locator).sendKeys(searchText3);
		driver.findElement(By.id("radio-button-2")).click();
		driver.findElement(By.id("checkbox-2")).click();
		driver.findElement(By.xpath("//*[@id=\"select-menu\"]/option[2]")).click();
		driver.findElement(By.id("datepicker")).click();
		driver.findElement(By.className("today")).click();
		driver.findElement(By.cssSelector("a[role='button']")).click();
		
		Thread.sleep(300);
		assertTrue(formPage.submit());
		submissionPage = new SubmissionPage(driver);
		assertEquals("The form was successfully submitted!", submissionPage.getSubmisionMessage());

	}
}

