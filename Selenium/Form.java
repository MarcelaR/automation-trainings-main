package E2;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Form {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver","C:\\Selenium downloads\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://formy-project.herokuapp.com/form");
		driver.manage().window().maximize();
		driver.findElement(By.id("first-name")).sendKeys("Marcela");
		driver.findElement(By.id("last-name")).sendKeys("Ruiz");
		driver.findElement(By.id("job-title")).sendKeys("IT Trainee");
		driver.findElement(By.id("radio-button-2")).click();
		driver.findElement(By.id("checkbox-2")).click();
		driver.findElement(By.xpath("//*[@id=\"select-menu\"]/option[2]")).click();
		driver.findElement(By.id("datepicker")).click();
		driver.findElement(By.className("today")).click();
		driver.findElement(By.cssSelector("a[role='button']")).click();
		
		driver.close();

	}

}
