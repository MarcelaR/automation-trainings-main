package introduction;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class e4 {
	
	WebDriver driver;
	
	By firstN = By.id("first-name");
	By lastN = By.id("last-name");
	By jobT = By.id("job-title");
	By eduL = By.id("radio-button-2");
	By sex = By.id("checkbox-2");
	By yearsE = By.xpath("//*[@id=\"select-menu\"]/option[2]");
	By calendar = By.id("datepicker");
	By todayD = By.className("today");
	By subA = By.cssSelector("a[role='button']");
	
	public e4(WebDriver driver)
	{
		this.driver = driver; //initialize driver
	}
	
	public void fillTheForm(WebDriver driver){
		driver.findElement(firstN).sendKeys("ela"); 
		driver.findElement(lastN).sendKeys("Ruiz"); 
		driver.findElement(jobT).sendKeys("IT Trainee"); 
		driver.findElement(eduL).click(); 
		driver.findElement(sex).click(); 
		driver.findElement(yearsE).click(); 
		driver.findElement(calendar).click(); 
		driver.findElement(todayD).click(); 
		driver.findElement(subA).click();
	}
	
}
