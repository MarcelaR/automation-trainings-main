/**
 * 
 */
package introduction;


import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class verifyTestcasesForm {
	
	@Test
	public void testFillForm()
	{
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://formy-project.herokuapp.com/form");
		
		e4 sub = new e4(driver);
		sub.fillTheForm(driver);
		
		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div[role='alert']")));
		assertEquals(driver.findElement(By.cssSelector("[align='center']")).getText(),"Thanks for submiting your form.");

		driver.quit();
	}
}
