package demop.test_ecommerce;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class Test_AddRemove extends Base_Test{
	
	//Cart Locator
	By cart = By.className("shopping_cart_link");
	//Product Locators
	By backpack = By.id("add-to-cart-sauce-labs-backpack");
	By bikelightImage = By.xpath("//*[@id=\"item_0_img_link\"]/img");
	By bikeLight = By.id("add-to-cart-sauce-labs-bike-light");
	By onesie = By.id("add-to-cart-sauce-labs-onesie");
	By jacket = By.id("add-to-cart-sauce-labs-fleece-jacket"); 
	
	@Test //Test case 1: Add Items
    public void add() throws InterruptedException {
		//Add Item 1: Backpack (from main products page)
		click(backpack);
		Thread.sleep(1000);
		Ss.takeScreenshot(driver, "Add_001_01");
		//Add Item 2: Bike light (from specific product page)
		click(bikelightImage);  													//Go to product page clicking product image
		Ss.takeScreenshot(driver, "Add_001_02");
		driver.findElement(By.id("add-to-cart-sauce-labs-bike-light")).click();     //Add product to cart
		Ss.takeScreenshot(driver, "Add_001_03");
		driver.findElement(By.id("back-to-products")).click(); 						//Go back to main products page
		Ss.takeScreenshot(driver, "Add_001_04");
		//Go to cart
		click(cart); 
		//Verify item 1: Backpack By Name
		WebElement strValue1 = driver.findElement(By.id("item_4_title_link"));
		String strExpected1 = "Sauce Labs Backpack";
		String strActual1 = strValue1.getText();
		System.out.println("Epected Item: "+strExpected1);
		System.out.println("Actual  Item: "+strActual1);
		if (strExpected1.equals(strActual1)) {
			System.out.println("--- Correct item added ---");
		} else {
			System.out.println("--- Incorrect item added ---");
		}
		//Verify item 2: Bike light By Price
		WebElement strValue2 = driver.findElement(By.xpath("//*[@id=\"cart_contents_container\"]/div/div[1]/div[4]/div[2]/div[2]/div"));
		String strExpected2 = "$9.99";
		String strActual2 = strValue2.getText();
		System.out.println("Epected Cost: "+strExpected2);
		System.out.println("Actual  Cost: "+strActual2);
		if (strExpected2.equals(strActual2)) {
			System.out.println("--- Correct item added ---");
		} else {
			System.out.println("--- Incorrect item added ---");
		}
		Ss.takeScreenshot(driver, "Add_001_05");
		System.out.println("Test case: Add_001 ---> Done");
	}
	@Test //Test case 2: Remove Items
    public void Remove() {
		//Add Item 1: Onesie
		click(onesie); 
		Ss.takeScreenshot(driver, "Remove_001_01");
		//Add Item 2: Jacket
		click(jacket); 
		Ss.takeScreenshot(driver, "Remove_001_02");
		//Go to cart
		click(cart); 
		//Verify item 1: onesie
		String expectedItem1 = new String("Sauce Labs Onesie");
		String actualItem1 = driver.findElement(By.xpath("//*[@id=\"item_2_title_link\"]/div")).getText();
		System.out.println(actualItem1);
		System.out.println(expectedItem1.equals(actualItem1));// returns true if correct item was added
		//Verify item 2: jacket
		String expectedItem2 = new String("Sauce Labs Fleece Jacket");
		String actualItem2 = driver.findElement(By.xpath("//*[@id=\"item_5_title_link\"]/div")).getText();
		System.out.println(actualItem2);
		System.out.println(expectedItem2.equals(actualItem2));// returns true if correct item was added
		Ss.takeScreenshot(driver, "Remove_001_03");
		//Remove Item
		driver.findElement(By.id("remove-sauce-labs-onesie")).click(); 
		Ss.takeScreenshot(driver, "Remove_001_04");
		System.out.println("Test case: Remove_001 ---> Done");
	}

}
