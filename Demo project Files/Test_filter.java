package demop.test_ecommerce;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class Test_filter extends Base_Test{
	
	//Filter Locators
	By filterBtn = By.className("product_sort_container");
	By option1 = By.xpath("//*[@id=\"header_container\"]/div[2]/div[2]/span/select/option[1]");
	//Products Locators
	By onesie = By.xpath("//*[@id=\"item_2_title_link\"]/div");
	By redTshirt = By.xpath("//*[@id=\"item_3_title_link\"]/div");
	By jacket = By.xpath("//*[@id=\"item_5_title_link\"]/div");
	By backpack = By.xpath("//*[@id=\"item_4_title_link\"]/div");
	
	@Test //Test case 1: Get list of all options
    public void sortByOptions() throws InterruptedException {
		//Get list of sort options
		WebElement element = findElement(filterBtn);
		Select select = new Select(element);
		List<WebElement> list = select.getOptions();
		System.out.println("You can Sort by:");
		for(int i=0; i<list.size(); i++) {
		        System.out.println(list.get(i).getText());
		}
		WebElement filter = findElement(filterBtn); 
		Actions action1 = new Actions(driver); 
		action1.moveToElement(filter).click().build().perform();
		click(option1);
		Thread.sleep(2000); 
		Ss.takeScreenshot(driver, "SortBy_001");
	}
	
	@Test //Test case 2: Sort items by Price (Low to High)
	public void sortBy1() throws InterruptedException {
		//Sort by Price (Low to High)
		Select drpdown= new Select(findElement(filterBtn)); 
		drpdown.selectByValue("lohi");
		Thread.sleep(3000);
		//Assert first product shown
	    String firstProduct = getText(onesie);
	    Assert.assertEquals("Sauce Labs Onesie",firstProduct);
		Ss.takeScreenshot(driver, "SortBy_002");
		System.out.println("Test case: SortBy_001 ---> Done");
	}
	
	@Test //Test case 3: Sort items by Name (Z to A)
    public void sortBy2() throws InterruptedException {
		Select drpdown= new Select(findElement(filterBtn)); 
		drpdown.selectByIndex(1);
		Thread.sleep(3000);
		//Assert first product shown
	    String firstProduct = getText(redTshirt);
	    Assert.assertEquals("Test.allTheThings() T-Shirt (Red)",firstProduct);
		Ss.takeScreenshot(driver, "SortBy_003");
		System.out.println("Test case: SortBy_002 ---> Done");
	}
	
	@Test //Test case 4: Sort items by Name (High to Low)
    public void sortBy3() throws InterruptedException {
		Select drpdown= new Select(findElement(filterBtn)); 
		drpdown.selectByVisibleText("Price (high to low)");
		Thread.sleep(3000);
		//Assert first product shown
	    String firstProduct = getText(jacket);
	    Assert.assertEquals("Sauce Labs Fleece Jacket",firstProduct);
		Ss.takeScreenshot(driver, "SortBy_004");
		System.out.println("Test case: SortBy_003 ---> Done");
	}
	
	@Test //Test case 5: Sort items by Name (A to Z)
    public void sortBy4() throws InterruptedException {
		Select drpdown= new Select(findElement(filterBtn)); 
		drpdown.getFirstSelectedOption();
		//Assert first product shown
	    String firstProduct = getText(backpack);
	    Assert.assertEquals("Sauce Labs Backpack",firstProduct);
		Ss.takeScreenshot(driver, "SortBy_005");
		System.out.println("Test case: SortBy_004 ---> Done");
	}

}
