package demop.test_ecommerce;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class Ss {
	
	public static void takeScreenshot(WebDriver driver, String imageName) {
	try {
		TakesScreenshot ts = (TakesScreenshot)driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(source, new File("./Screenshots/"+imageName+".png"));
	} catch (Exception e){
		System.out.println("Exception while taking screenshot"+e.getMessage());
	}
	}
}
