package demop.test_ecommerce;

import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Page_orderFlow extends Base_orderFlow{
	
	//Login Locators
	By user = By.id("user-name");
	By psw = By.id("password");
	By loginBtn = By.id("login-button");
	//Item Locators
	By backpack = By.id("add-to-cart-sauce-labs-backpack");
	By actualItemBackpack = By.xpath("//*[@id=\"item_4_title_link\"]/div");
	By bikeLight = By.id("add-to-cart-sauce-labs-bike-light");
	By actualItemBikelight  = By.xpath("//*[@id=\"item_0_title_link\"]/div");
	By onesie = By.id("add-to-cart-sauce-labs-onesie");
	By actualItemOnesie  = By.xpath("//*[@id=\"item_2_title_link\"]/div");
	By jacket = By.id("add-to-cart-sauce-labs-fleece-jacket"); 
	By actualItemJacket  = By.xpath("//*[@id=\"item_5_title_link\"]/div");
	//Cart Locator
    By cart = By.className("shopping_cart_link");
	//Info section	Locators
    By qtySection = By.xpath("//*[@id=\"cart_contents_container\"]/div/div[1]/div[2]");
	By yourInfoTitle = By.xpath("//*[@id=\"header_container\"]/div[2]/span");
	By checkoutOrder = By.id("checkout");
	By continueOrder = By.id("continue");
	By fistName = By.id("first-name");
	By lastName = By.id("last-name");
	By postalCode = By.id("postal-code");
	//Total cost Locators
	By itemTotal = By.xpath("//*[@id=\"checkout_summary_container\"]/div/div[2]/div[5]");
	By total = By.xpath("//*[@id=\"checkout_summary_container\"]/div/div[2]/div[7]");
	By confirmationMessage = By.xpath("//*[@id=\"checkout_complete_container\"]/h2");
	By finishBtn = By.id("finish");
	
	double tax1 = 2.40;
	double tax2 = 5.44;
	
	public Page_orderFlow(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	public void login(String testNumber) throws InterruptedException{
		type("standard_user",user);
		type("secret_sauce",psw);
		takeSs("makeAnOrder_"+testNumber+"_01");
		click(loginBtn);
		Thread.sleep(2000);
		takeSs("makeAnOrder_"+testNumber+"_02");
	}
	public void AddItems(int numberOfItems_option, String testNumber) throws InterruptedException{
		if(numberOfItems_option == 1) {
			//Add One Item: Backpack
			click(backpack);  
			Thread.sleep(2000);
			takeSs("makeAnOrder_"+testNumber+"_03");
		}else if(numberOfItems_option == 2) {
			//Add three Items: Bike light, onesie and jacket
			click(bikeLight); 
			click(onesie);
			click(jacket);
			Thread.sleep(2000);
			takeSs("makeAnOrder_"+testNumber+"_03");
		}else {
			System.out.println("Invalid option...");
		}
			
	}
	public void goToCart() throws InterruptedException{
		click(cart);  
		Thread.sleep(2000);	
	}
	public void verifyCart(int itemsToVerify_option, String testNumber) throws InterruptedException {
		if(itemsToVerify_option == 1) {
			//Verify Item: Backpack
			String expectedItem1 = new String("Sauce Labs Backpack");
			String actualItemS = getText(actualItemBackpack);
			System.out.println(actualItemS);
			if(expectedItem1.equals(actualItemS)) { // returns true if correct item was added
				takeSs("makeAnOrder_"+testNumber+"_04");
				click(checkoutOrder);
			}else {
				System.out.println("--- Incorrect item added ---");
			}
		}else if(itemsToVerify_option == 2) {
			//Verify Item: Bike light
			String expectedItem1 = new String("Sauce Labs Bike Light");
			String actualItem1 = getText(actualItemBikelight);
			System.out.println(actualItem1);
			if(expectedItem1.equals(actualItem1)) { // returns true if correct item was added
				System.out.println("--- Correct item (1) added ---");
			}else {
				System.out.println("--- Incorrect item (1) added ---");
			}
			//Verify Item: Onesie
			String expectedItem2 = new String("Sauce Labs Onesie");
			String actualItem2 = getText(actualItemOnesie);
			System.out.println(actualItem2);
			if(expectedItem2.equals(actualItem2)) { // returns true if correct item was added
				System.out.println("--- Correct item (2) added ---");
			}else {
				System.out.println("--- Incorrect item (2) added ---");
			}
			//Verify Item: Jacket
			String expectedItem3 = new String("Sauce Labs Fleece Jacket");
			String actualItem3 = getText(actualItemJacket);
			System.out.println(actualItem3);
			if(expectedItem3.equals(actualItem3)) { // returns true if correct item was added
				System.out.println("--- Correct item (3) added ---");
			}else {
				System.out.println("--- Incorrect item (3) added ---");
			}
			//Screenshot
			scrollDown(qtySection);
			takeSs("makeAnOrder_"+testNumber+"_04");
			click(checkoutOrder);
		}else {
			System.out.println("Invalid option...");
		}
	}
	
	public void addClientInfoExl(int clientNumber, String testNumber) throws IOException {
		if (isDisplayed(yourInfoTitle)) {
			String filepath = "C:\\Users\\MRC\\Desktop\\Clients_Data.xlsx";
			String clientName = getCellValueS(filepath,"Sheet1", clientNumber, 0);
			type(clientName, fistName);
			String clientLastName = getCellValueS(filepath,"Sheet1", clientNumber, 1);
			type(clientLastName, lastName);
			int clientPostalCodeN = (int)getCellValueD(filepath,"Sheet1", clientNumber, 2);
			String clientPostalCodeS=String.valueOf(clientPostalCodeN);  
			type(clientPostalCodeS, postalCode);
			takeSs("makeAnOrder_"+testNumber+"_05");
			click(continueOrder);
		} else {
			System.out.println("YOUR INFORMATION PAGE not found");
		}
	}
	public void verifyTotal(int verifyTotal_option, String testNumber) {
		if(verifyTotal_option == 1) {
			//ItemTotal
				double backpackCostD = 29.99;
				String backpackCostS = String.valueOf(backpackCostD);
			assertTrue(getText(itemTotal).contains(backpackCostS));
				//Total + Tax
				double t = tax1 + backpackCostD;
			assertTrue(getText(total).contains(String.valueOf(t)));
			//Screenshot
			scrollDown(actualItemBackpack);
			takeSs("makeAnOrder_"+testNumber+"_06");
		}else if(verifyTotal_option == 2) {
			//ItemTotal
				//individual costs
				double bikelightCostD = 9.99;
				double onesieCostD = 7.99;
				double jacketCostD = 49.99;
				//Get and convert total before taxes to string
				double totalBeforeTaxesD = bikelightCostD + onesieCostD + jacketCostD;
				String totalBeforeTaxesS = String.valueOf(totalBeforeTaxesD);
			assertTrue(getText(itemTotal).contains(totalBeforeTaxesS));
				//Total + Tax
				double t = tax2 + totalBeforeTaxesD;
			assertTrue(getText(total).contains(String.valueOf(t)));
			//Screenshot
			scrollDown(actualItemOnesie);
			takeSs("makeAnOrder_"+testNumber+"_06");
		}else {
			System.out.println("Invalid option...");
		}
	}
	
	public void finishOrder(String testNumber) {
		click(finishBtn);
		String actualMessage = getText(confirmationMessage);
		System.out.println(actualMessage);
		Assert.assertEquals("THANK YOU FOR YOUR ORDER",actualMessage);
		scrollDown(confirmationMessage);
		takeSs("makeAnOrder_"+testNumber+"_07");
	}
}
		
	
	
			

