package demop.test_ecommerce;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

//import dp_pruebas.d.AppTest;

public class Tets_login {
	
	private WebDriver driver;
	private String url = "https://www.saucedemo.com/";
	private static final Logger logger = LogManager.getLogger(Tets_login.class);
	
	//Login Page Locators
	By user = By.id("user-name");
	By pwd = By.id("password");
	By loginBtn = By.id("login-button");
	By errorText = By.xpath("//*[@id=\"login_button_container\"]/div/form/div[3]/h3");
	
	@Before //Sets WebDriver Chrome to run all tests and opens e-commerce web page
	public void setdriver() {
	 System.setProperty("webdriver.chrome.driver", "C:\\Users\\MRC\\Desktop\\Demo project Files\\chromedriver.exe");
	 driver = new ChromeDriver();
	 logger.info("Navigate to {}", 
 			url,((RemoteWebDriver)driver).getCapabilities());
 	driver.navigate().to(url);
	}
	
	@After //Closes test pages
	public void closePage() {
		logger.info("closing the driver");
		driver.quit();
	}
	
	@Test //Test case 1: Login using valid username and valid password
    public void login1() {
		driver.findElement(user).sendKeys("standard_user");
		driver.findElement(pwd).sendKeys("secret_sauce");
		Ss.takeScreenshot(driver, "Login_001_01");
		driver.findElement(loginBtn).click();
		Ss.takeScreenshot(driver, "Login_001_02");
		//Assert Home page title
	    String actualPage = driver.getTitle();
	    System.out.println(actualPage);
	    Assert.assertEquals("Swag Labs",actualPage);
	    System.out.println("Verify Page Title: Done");
		System.out.println("Test case: Login_001 ---> Done");
	}
	
	@Test //Test case 2: Login using valid username and invalid password
	public void login2() {
		driver.findElement(user).sendKeys("standard_user");
		driver.findElement(pwd).sendKeys("**");
		driver.findElement(loginBtn).click();
		String actualMessage = driver.findElement(errorText).getText();
        System.out.println(actualMessage);
        Assert.assertEquals("Epic sadface: Username and password do not match any user in this service",actualMessage);
        logger.warn("Use of Invalid password");
        Ss.takeScreenshot(driver, "Login_002");
		System.out.println("Test case: Login_002 ---> Done");
	}
	
	@Test //Test case 3: Login using invalid username and valid password
	public void login3() {
		driver.findElement(user).sendKeys("wrong_user");
		driver.findElement(pwd).sendKeys("secret_sauce");
		driver.findElement(loginBtn).click();
		String actualMessage = driver.findElement(errorText).getText();
        System.out.println(actualMessage);
        Assert.assertEquals("Epic sadface: Username and password do not match any user in this service",actualMessage);
        logger.warn("Use of invalid username");
        Ss.takeScreenshot(driver, "Login_003");
        System.out.println("Test case: Login_003 ---> Done");
	}
	
	@Test //Test case 4: Login no entering data
	public void login4() {
		driver.findElement(loginBtn).click();
		String actualMessage = driver.findElement(errorText).getText();
        System.out.println(actualMessage);
        Assert.assertEquals("Epic sadface: Username is required",actualMessage);
        logger.warn("Skip entering data consequence");
        Ss.takeScreenshot(driver, "Login_004");
        System.out.println("Test case: Login_004 ---> Done");
	}

}