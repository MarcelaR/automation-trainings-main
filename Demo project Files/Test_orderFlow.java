package demop.test_ecommerce;

import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

public class Test_orderFlow {
	
	private WebDriver driver;
	Page_orderFlow page_orderFlow;
	
	@Before
	public void setUp() throws InterruptedException {
		page_orderFlow = new Page_orderFlow(driver);
		driver = page_orderFlow.chromeDriverConnection();
		page_orderFlow.visit("https://www.saucedemo.com/");
	}
	
	@After
	public void tearDown() {
		driver.quit();
	}
    @Test
    public void makeAnOrder1() throws InterruptedException, IOException
    {
    	page_orderFlow.login("001");                //test number for Ss name
    	page_orderFlow.AddItems(1,"001");			//option number, test number for Ss name
    	page_orderFlow.goToCart();
    	page_orderFlow.verifyCart(1,"001");         //option number, test number for Ss name
    	page_orderFlow.addClientInfoExl(2,"001");   //client number, test number for Ss name
    	page_orderFlow.verifyTotal(1,"001");        //option number, test number for Ss name
    	page_orderFlow.finishOrder("001");          //test number for Ss name
    }
   @Test
    public void makeAnOrder2() throws InterruptedException, IOException
    {
	    page_orderFlow.login("002");                //test number for Ss name
	    page_orderFlow.AddItems(2,"002");			//option number, test number for Ss name
	   	page_orderFlow.goToCart();
	   	page_orderFlow.verifyCart(2,"002");			//option number, test number for Ss name
	   	page_orderFlow.addClientInfoExl(3,"002");   //client number, test number for Ss name
	   	page_orderFlow.verifyTotal(2,"002");        //option number, test number for Ss name
	   	page_orderFlow.finishOrder("002");          //test number for Ss name
    }
}
