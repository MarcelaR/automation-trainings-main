package demop.test_ecommerce;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Base_Test {
	
	WebDriver driver;
	
	@Before //Sets WebDriver Chrome to run all tests, opens e-commerce web page and logs in
	public void setPage() {
	 System.setProperty("webdriver.chrome.driver", "C:\\Users\\MRC\\Desktop\\Demo project Files\\chromedriver.exe");
	 driver = new ChromeDriver();
	 driver.get("https://www.saucedemo.com/");
	 driver.manage().window().maximize();
	 driver.findElement(By.id("user-name")).sendKeys("standard_user");
	 driver.findElement(By.id("password")).sendKeys("secret_sauce");
	 driver.findElement(By.id("login-button")).click();
	}
	
	@After //Closes test pages
	public void closePage() {
		driver.quit();
	}
	
	//methods
	public  void click(By locator) {
		driver.findElement(locator).click();
	}
	public void type(String inputText, By locator) {
		driver.findElement(locator).sendKeys(inputText);
	}
	public WebElement findElement(By locator) {
		return driver.findElement(locator);
	}
	public String getText(By locator) {
		return driver.findElement(locator).getText();
	}
}
